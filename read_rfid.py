#!/usr/bin/python
# by Openscop
# Last update : 25/06/2013

import serial
import urllib.request as urllib

from serial.tools import list_ports


url_post = "http://10.78.34.100:8000/"




port = [port[0] for port in list_ports.comports()]
print(port)

def read():
    ser = serial.Serial('/dev/ttyUSB0', 2400, timeout=0.5)
    string = ser.read(12)[2:-1].decode('utf-8')
    return string


while 1:
    try:
        tagId = read()
        if tagId != "":
            print(tagId)

            try:
                urllib.urlopen(url_post + "badge/tag/" + tagId)
            except urllib.URLError:
                print("I can't contact the server")
                pass

    except serial.SerialException:
        string = "Error with RFID tag reader"
        print(string)
        break
