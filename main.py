#!/usr/bin/python
# by Openscop
# Last update : 25/06/2013

import serial
import urllib.request as urllib
import requests
import json

from serial.tools import list_ports

url_serveur = "http://fabaccess.openscop.tech/api/"

class DataRequest():
    def __init__(self, id_equipement, numero_tag):
        self.id_equipement = id_equipement
        self.numero_tag = numero_tag

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
            sort_keys=True, indent=4)

port = [port[0] for port in list_ports.comports()]
print(port)

def read():
    ser = serial.Serial('/dev/ttyUSB0', 2400, timeout=0.5)
    string = ser.read(12)[2:-1].decode('utf-8')
    return string


while 1:
    try:
        tagId = read()
        if tagId != "":
            print("tag " + tagId)

            try:
                post_data = DataRequest("123", tagId)
                r = requests.post(url_serveur + 'check/', data=post_data.toJSON(), timeout=5)
                r.raise_for_status()
                print(r.text)

            except requests.exceptions.HTTPError as errh:
                print("Http Error:", errh)
            except requests.exceptions.ConnectionError as errc:
                print("Error Connecting:", errc)
            except requests.exceptions.Timeout as errt:
                print("Timeout Error:", errt)
            except requests.exceptions.RequestException as err:
                print("OOps: Something Else", err)

    except serial.SerialException:
        string = "Error with RFID tag reader"
        print(string)
        break
